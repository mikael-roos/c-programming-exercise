# C-programming-exercise

Exercises to C programming.

The lecture material is [available in GitLab Pages as a website](https://mikael-roos.gitlab.io/c-programming-exercise/).

The source code with example and sample programs are available in the [`src/` directory](src).

Here is a [Question and Answer](questions-and-answers) area that might be of help to you.

/Mikael
