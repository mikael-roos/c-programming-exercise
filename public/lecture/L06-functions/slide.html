<!doctype html>
<html class="theme-5">
<meta charset="utf-8" />
<link href="../html-slideshow.bundle.min.css" rel="stylesheet" />
<link href="../style.css" rel="stylesheet" />
<script src="https://dbwebb.se/cdn/js/html-slideshow_v1.1.0.bundle.min.js"></script>

<title>Programming in C</title>

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Programming in C
## Functions
### Mikael Roos
</script>



<script data-role="slide" type="text/html" data-markdown>
# Agenda

* Functions
* Ch 4 "Functions and program structure" from the book "The C Programming Language"

</script>



<script data-role="slide" type="text/html" data-markdown>
# About functions

* To break programs into smaller pieces
* Return values
* In and out parameters
* Be DRY and organise code in functions
* Organise in "modules" by separate code into files
* Header files versus source files
    * Include file to declare the function
    * Source file to implement the function

</script>


<script data-role="slide" type="text/html" data-markdown>
# Basics of main function

```
#include <stdio.h>

int main (int argc, char *argv[])
{
    printf("Have a nice weekend!\n");

    return(42);
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Basics of main...

```
$ ./main
Have a nice weekend!

$ echo $?
42
```

<p class="footnote">The '$?' is Unix. Try 'echo $LASTEXITCODE' on Windows PowerShell.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Create a function

```
int printHello()
{
    printf("Have a nice weekend!\n");
    return(42);
}

int main (int argc, char *argv[])
{
    int status;

    status = printHello();
    return(status);
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Declare a function

```
int printHello();

int main (int argc, char *argv[])
{
    return(printHello());
}

int printHello()
{
    printf("Have a nice weekend!\n");
    return(42);
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Include a function

* `main.c`

```
#include "hello-world.h"

int main (int argc, char *argv[])
{
    return(printHello());
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Include a function...

* `hello-world.h`

```
int printHello();
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Include a function...

* `hello-world.c`

```
#include <stdio.h>

#include "hello-world.h"

int printHello()
{
    printf("Have a nice weekend!\n");
    return(42);
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Include a function...

```
$ make
gcc -g -Wall   -c -o hello-world.o hello-world.c
gcc -g -Wall   -c -o main.o main.c
gcc -g -Wall  -o main ./hello-world.o ./main.o
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Thoughts on functions

* Keep your `main` clean
* Keep functions small, like 5-20 lines or so
* Large functions can be organised into several smaller ones
* Do not repeat code, use functions instead
* A function should do one thing, not many things

</script>



<script data-role="slide" type="text/html" data-markdown>
# Thoughts on modules and libs

* Divide functions into "modules/libs" using `module.h` and `module.c`
* Try to keep modules self contained and isolated
* Link in modules as `module.o`
* Create a `libmodule.so` and link with `-lmodule`

</script>



<script data-role="slide" type="text/html" data-markdown>
# How to build a libhello.so

```
$ make
gcc -g -Wall   -c -o main.o main.c
gcc -c -Wall -Werror -fpic hello-world.c
gcc -shared -o libhello.so hello-world.o
gcc -g -Wall  -o main main.o -L. -lhello
```

<p class="footnote">Libs are compiled with position independant code PIC ('-fpic').<br>The linker gets the path to the libs '-L.'.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Run with lib

```
$ ./main
./main: error while loading shared libraries: libhello.so:
 cannot open shared object file: No such file or directory

$ LD_LIBRARY_PATH="." ./main
Have a nice weekend!
```

<p class="footnote">The executable can be linked static or using shared libraries.<br>Note that this example is made for Unix where we use 'LD_LIBRARY_PATH' to find where the libraries are.</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Back to functions
</script>



<script data-role="slide" type="text/html" data-markdown>
# Basics of functions

```
// Smallest function, return void/nothing
void dummy () {}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Functions returning

```
// May return value
int dummy () {
    return 42;
}

// Type whats returned
float dummy () {
    return 3.141592654;
}

char dummy () {
    return 'Z';
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Arguments and parameters

* PARAMETER → PLACEHOLDER
* Parameter is a variable in the declaration or call of function.
* ARGUMENT → ACTUAL VALUE
* When a method is called, the arguments are the data you pass into the method's parameters.

</script>



<script data-role="slide" type="text/html" data-markdown>
# Arguments and parameters...

```
int sum (int a, int b, int c) {
    return a + b + c;
}

int main ()
{
    int k = 1;
    int l = 2;
    int m = 3;

    printf("Sum is %d\n", sum(k, l, m));
}
```

<p class="footnote">Parameters are passed by value.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Pass by address

```
void update (int* a, int* b) {
    (*a)++;
    (*b)--;
}

int main ()
{
    int k = 2;
    int l = 4;

    update(&k, &l);
    update(&k, &l);

    printf("values are %d %d\n", k, l);
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Pass by reference?

> The C language is pass-by-value without exception. Passing a pointer as a parameter does not mean pass-by-reference.

* A function is not able to change the actual parameters value.

</script>



<script data-role="slide" type="text/html" data-markdown>
# Variable parameter list

```
int sum (int argc, ...);

int main ()
{
    int k = 1;
    int l = 2;
    int m = 3;

    printf("Sum is %d\n", sum(5, k, l, m, 6, 12));
}
```

<p class="footnote">Compare to printf and scanf.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Variable parameter list...

```
#include <stdarg.h>

int sum (int argc, ...) {
    int aSum = 0;
    va_list ap;                     # declare argument pointer

    va_start(ap, argc);             # init the ap
    for(int i=0; i< argc; i++) {    # loop through all arguments
        aSum += va_arg(ap, int);    # get/access each argument
    }
    va_end(ap);                     # end the argument pointer

    return aSum;
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Scope rules

* Declarations in functions has function scope
* Declarations on file level visible for functions in that file
    * global variable
* Declare `extern` to allow for a variable to be declared somewhere else


</script>



<script data-role="slide" type="text/html" data-markdown>
# External variables

```
#include "module.h"

const char* str_MAIN = "main.c";

extern char* str_MODULE_C;

int main ()
{
    printf("%s\n", str_MAIN);
    printf("%s\n", str_MODULE_H);
    printf("%s\n", str_MODULE_C);
}
```

<p class="footnote">This is main.c.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# External variables...

```
#ifndef MODULE_H
#define MODULE_H

extern const char* str_MODULE_H;

void someFunc();

#endif
```

<p class="footnote">This is module.h, it makes the str_MODULE_H available for all who includes this file.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# External variables...

```
#include "module.h"

const char* str_MODULE_H = "module.h";
const char* str_MODULE_C = "module.c";

extern char* str_MAIN;

void someFunc () {
    printf("%s\n", str_MAIN);
    printf("%s\n", str_MODULE_H);
    printf("%s\n", str_MODULE_C);
}
```

<p class="footnote">This is module.c, it makes the str_MODULE_C available for all who includes this file and provides a value to str_MODULE_H.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Static local variables

* Initialize once, remember its value

```
void memory () {
    static int inc = 0;

    printf("%d\n", inc += 10);
}

int main (int argc, char *argv[])
{
    for (int i = 0; i < 7; i++) {
        memory();
    }
}
```

<p class="footnote">What is the output of this program?</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Static local variables

```
$ ./main
10
20
30
40
50
60
70
```

<p class="footnote">Static initializes once and then remembers its value.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Static file variables

* Restrain the visibility to file level

```
# In file main.c
extern char* str_MODULE_C;
```

```
# In file module.c
//const char* str_MODULE_C = "module.c";
static const char* str_MODULE_C = "module.c";
```

<p class="footnote">Static will protect the visibility to the file only. It also applies to functions.<br>It hides the visibility to make a file scope only.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Static hides token

* Functions and variables on file level are global objects in the symbol table
* Declare variables and functions as static, makes them local to the file they are declared in
* Avoids clashing variable and function names
* Good practice for declaring module internal functions and variables

</script>



<script data-role="slide" type="text/html" data-markdown>
# Register variables

* Advice the compiler that the variable is to be heavily used in calculations

```
register int x;
register int y;
```

<p class="footnote">You cannot use the address of a register variable.<br>Register is machine dependent and is a suggestion to the compiler which decides.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Block structure

* Functions contains blocks of code
* Blocks are `{ statements }`
* Each block introduces a new scope
* Variables declared in a function or block are local to that block or function

<p class="footnote">Functions can not contain functions.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Block scope

* Variables has block scope

```
int main ()
{
    int i = 42;

    printf("Outside %d\n", i);

    for (int i = 0; i <= 2; i++) {
        printf("Inside %d\n", i);
    }

    printf("Outside %d\n", i);
}
```

<p class="footnote">This program has two 'i' variables in the same function but in different blocks.<br>Can you figure out what is printed from executing the program?</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Block scope...

```
Outside 42
Inside 0
Inside 1
Inside 2
Outside 42
```

<p class="footnote">Variables in different blocks are individual variables.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Initialization

* When uninitialized
    * External and static variables guaranteed to be initialized to 0
    * Automatic and register variables have undefined (garbage) values
* External/static must be intialized with a constant expression
    * Only initialized once (before program execution)
* Automatic/register can be initialized with an expression
    * Initialized each time the block is entered

</script>



<script data-role="slide" type="text/html" data-markdown>
# Recursion

* A function calling itself
* Each new call is put onto the stack
* Consider the Fibonacci sequence

>  0, 1, 1, 2, 3, 5, 8, 13 and 21.

> Each number is the sum of the two preceeding numbers, starting from 0 and 1.

</script>



<script data-role="slide" type="text/html" data-markdown class="center">
# Fibonacci sequence

<figure>
<img src="img/fibonacci.png" width="80%">
<figcaption>Can we do a recursive algorithm to calculate this?</figcaption>
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Fibonacci sequence...

```
int  main() {

    int i;

    // Calculate the Fibonacci sequence for the first 10 numbers
    // in the sequence.
   for (i = 0; i < 10; i++) {
      printf("%d\t\n", fibonacci(i));
   }

   return 0;
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Fibonacci sequence...

```
int fibonacci(int i) {

   if(i == 0) {
      return 0;
   }

   if(i == 1) {
      return 1;
   }

   return fibonacci(i-1) + fibonacci(i-2);
}
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# Fibonacci sequence...

```
$ ./main
0
1
1
2
3
5
8
13
21
34
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# The C preprocessor

* The first compilation step, results in a ".c code"
* Include the files
    * `#include <file.h>`
    * `#include "file.h"`
* Macro substitution
    * `#define name replace`
* Things can be `#undef` (good to know)

</script>



<script data-role="slide" type="text/html" data-markdown>
# The C preprocessor...

* Conditional inclusion

```
#ifndef MODULE_H
#define MODULE_H

extern const char* str_MODULE_H;

void someFunc();

#endif
```

<p class="footnote">Define a token to avoid double inclusion.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Macros

```
#define max(A, B)   ((A) > (B) ? (A) : (B))
```

```
int a = max(7, 9);
```

```
int a = ((7) > (9) ? (7) : (9));
```

</script>



<script data-role="slide" type="text/html" data-markdown>
# C preprocessor

```
#define max(A, B)   ((A) > (B) ? (A) : (B))

int main() {
    int a = max(7, 9);
}
```

<p class="footnote">Use the compiler flag -E to get the output from the precompiler.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# C preprocessor...

```
$ gcc -E main.c
# 1 "main.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "main.c"

int main() {
    int a = ((7) > (9) ? (7) : (9));
}
```

<p class="footnote">Use the compiler flag -E to get the output from the precompiler.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Stack

* When a function is called, the data about the call is pushed to the stack
* Data being:
    * location of the call in your program
    * arguments of the call
    * local variables of the function being called
* This block of data is called a "stack frame"
* The "stack frames" are allocated on the "stack"

<p class="footnote">Use the debugger to examin the content of the stack.</p>

</script>



<script data-role="slide" type="text/html" data-markdown>
# Call B() from A()

* Lets review a sample function call to see what happens

> Calling a function B from function A on a typical "generic" system might involve the following steps...

</script>



<script data-role="slide" type="text/html" data-markdown>
# Call B() from A()...

* function A:
    * push space for the return value
    * push parameters
    * push the return address
* jump to the function B

</script>



<script data-role="slide" type="text/html" data-markdown>
# Call B() from A()...

* function B:
    * push the address of the previous stack frame
    * push values of registers that this function uses (to be restored)
    * push space for local variables
    * do the necessary computation
    * restore the registers
    * restore the previous stack frame
    * store the function result
    * jump to the return address

</script>



<script data-role="slide" type="text/html" data-markdown>
# Call B() from A()...

* function A:
    * pop the parameters
    * pop the return value

</script>



<script data-role="slide" type="text/html" data-markdown>
# Sum it up

* About functions
    * Arguments and parameters
    * Return values
    * Block structure and scope
    * Recursion
* The preprocessor
* The stack

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# The end
</script>



<script data-role="slide" type="text/html" data-markdown>
</script>

</html>
