
Formatting Your Source Code (GNU style)
https://www.gnu.org/prep/standards/standards.html#Formatting
https://en.wikipedia.org/wiki/GNU_coding_standards

Linux kernel coding style
https://www.kernel.org/doc/html/v4.10/process/coding-style.html


Reading Code From Top to Bottom
http://iq0.com/notes/deep.nesting.html

C data types:
https://en.wikipedia.org/wiki/C_data_types

C programming language
https://en.wikipedia.org/wiki/C_(programming_language)

Makefile

GCC compiler
https://www.gnu.org/software/gcc/

gdb
https://www.gnu.org/software/gdb/

Development tools

pyramid functions?

ELF
https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
https://upload.wikimedia.org/wikipedia/commons/e/e4/ELF_Executable_and_Linkable_Format_diagram_by_Ange_Albertini.png
https://wiki.osdev.org/ELF
https://linux-audit.com/elf-binaries-on-linux-understanding-and-analysis/

Memory Layout of C Programs
https://www.geeksforgeeks.org/memory-layout-of-c-program/

Exercises:

argc and argv
https://www.gnu.org/software/libc/manual/html_node/Program-Arguments.html

exit status
https://www.gnu.org/software/libc/manual/html_node/Program-Termination.html

ELF
https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
