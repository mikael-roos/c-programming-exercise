#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    char strA[] = "Hello world!";
    char *strP  = "Hello world!";
    int i;

    for (i = 0; strA[i]; i++) {
        printf("%c", strA[i]);
    }
    printf("\n");

    char* tmp = strP;
    char ch;
    while ((ch = *strP++)) {
        printf("%c", ch);
    }
    printf("\n");

    strP = tmp;
    while (*strP != '\0') {
        ch = *strP;
        printf("%c", ch);
        strP += 1;
    }
    printf("\n");

    exit(EXIT_SUCCESS);
}
