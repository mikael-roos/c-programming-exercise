#include <stdio.h>
#include <stdlib.h>

char* get_month_name (int i) {
    static char *months[] = {
        "Illegal month", "January", "February",
        "March", "April", "May", "June",
        "July", "August", "September", "October",
        "November", "December"
    };

    return (i > 0 && i <= 12 ? months[i] : months[0]);
}

int main (int argc, char *argv[])
{
    int monthNum;
    char* monthStr;

    monthNum = 7;
    monthStr = get_month_name(monthNum);
    printf("%d represents month '%s'.\n", monthNum, monthStr);

    exit(EXIT_SUCCESS);
}
