#include <stdio.h>
#include <stdlib.h>

void swap (int *a, int *b)
{
    int temp = *a;

    *a = *b;
    *b = temp;
}

int main (int argc, char *argv[])
{
    int a = 7;
    int b = 9;

    printf("a = %d AND b = %d.\n", a, b);

    swap(&a, &b);

    printf("a = %d AND b = %d.\n", a, b);

    exit(EXIT_SUCCESS);
}
