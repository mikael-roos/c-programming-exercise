#include <stdio.h>
#include <stdlib.h>

#define LINES 5

int main (int argc, char *argv[])
{
    int line;
    char *document[LINES] = {
        "This is the first line in the document",
        "This is the second line...",
        "",
        "The line above is an empty line.",
        "This is the last line!"
    };

    for (line = 0; line < LINES; line++) {
        printf("%s\n", document[line]);
    }

    exit(EXIT_SUCCESS);
}
