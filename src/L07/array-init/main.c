#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    int fib[7] = {0, 1, 1, 2, 3, 5, 8};

    for (int i = 0; i < 7; i++) {
        printf("%d\n", fib[i]);
    }

    exit(EXIT_SUCCESS);
}
