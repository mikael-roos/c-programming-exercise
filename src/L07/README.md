Homework assignment
=======================

This is the voluntary homework assignment that will let you practice small exercises and learn C programming from examples.

[[_TOC_]]



Try out and inspect the example programs
-----------------------

Try out the example programs in this folder. Compile them, exeute them.

Inspect their source code, can you understand all parts of the code and can you understand what the program is doing?

Feel free to copy them and try to make your own updates to them.



Add features to your bot
-----------------------

Improve your program by adding more features to your bot program. Try to add the following features.

```
$ ./a.out
This is your personal bot "Marvin" that can anwer anything.
     (____)
     (_oo_)
       (O)
     __||__     )
  []/_______[] /
  / ________/ /
 /    /___/
(    /___/

Menu
-----------------------------------------

11. Calculate the length of a string (length)
12. Reverse a string (reverse)
13. Calculate the words in a string (words)
14. Print statistics on a text (stats)
15. Character frequence (freq)

-----------------------------------------

Enter your choice (or 'q' to quit):
```

Store all these functions in their own module using `.h` and `.c` files.



### 11. Calculate the length of a string

Ask the user for a string and caclulate the length of the string by using a loop that walks through the string.



### 12. Reverse a string

Ask the user for a string and then print it out including how it looks like when it is reversed.



### 13. Calculate the words in a string

Ask the user for a string (a text including spaces) and then count the words in the text and print out the text and the number of characters in it (excluding spaces) and the numbers of words in it.



### 14. Print statistics on a text

Ask the user for a string (a text including spaces, digits and special charactes) and then count and print the following statistics on the text.

* Number of ordinary characters [a-Z] or [a-Ö]
* Number of consonants
* Number of vowels
* Number of digits
* Number of special characters



### 15. Character frequence

Ask the user for a string (a text including spaces, digits and special charactes) and then count the number of occurences for each character in the text. Print out the characters and their frequence. Make it harder to only print the top ten characters that has the highest character frequence in the text.



### Update how one enters the menu choice

After each menu choice 11-15 there is a string representing that menu choice. Update how the user chooses the menu choice by allowing the user to enter these strings to choose the actual menu choice.

That is, if the user enters `freq` then the program executes the menu choice 15 that calculates the character frequence in a text.
