#include <stdio.h>
#include <stdlib.h>

int fibonacci(int i) {

   if(i == 0) {
      return 0;
   }

   if(i == 1) {
      return 1;
   }

   return fibonacci(i-1) + fibonacci(i-2);
}

int main (int argc, char *argv[])
{
    int i;
    const int size = 10;
    int fib[size];
    int *p = &fib[0];

    for (i = 0; i < size; i++) {
        fib[i] = fibonacci(i);
    }

    for (i = 0; i < size; i++) {
        printf("%d\n", fib[i]);
    }

    for (i = 0; i < size; i++) {
        int *pfib = p + i;

        printf("%d\n", *pfib);
    }

    exit(EXIT_SUCCESS);
}
