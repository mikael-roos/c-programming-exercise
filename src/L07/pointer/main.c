#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    int  a;         // A integer variable a
    int* p;         // A pointer to a integer

    p = &a;         // Now p contains the address of a

    a = 40;         // The value of a and *p is now 40
    *p += 2;        // The value of a and *p is now 42

    printf("The value of   a = %d.\n", a);
    printf("The value of  *p = %d.\n", *p);

    printf("The address of the variable         &a = %p.\n", &a);
    printf("The address stored in the pointer    p = %p.\n", p);
    printf("The address of the pointer variable &p = %p.\n", &p);

    exit(EXIT_SUCCESS);
}
