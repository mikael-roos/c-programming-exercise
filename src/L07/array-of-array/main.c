#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    char board[3][3] = {
        {'_', '_', '_'},
        {'_', '_', '_'},
        {'_', '_', '_'}
    };

    board[1][1] = 'X';
    board[0][2] = '0';
    board[2][1] = 'X';

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%c", board[i][j]);
        }
        printf("\n");
    }

    exit(EXIT_SUCCESS);
}
