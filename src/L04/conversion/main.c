#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    short a = 1;
    int b = 2;
    float c = 3.14;
    char d = 4;

    a = a + b;
    printf("a = %d\n", a);

    a = a + b + c;
    printf("a = %d\n", a);

    a = a + b + c + d;
    printf("a = %d\n", a);

    exit(EXIT_SUCCESS);
}
