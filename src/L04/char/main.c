#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    printf("  CHAR_BIT: %12d\n", CHAR_BIT);
    printf(" SCHAR_MIN: %12d\n", SCHAR_MIN);
    printf(" SCHAR_MAX: %12d\n", SCHAR_MAX);
    printf("  CHAR_MIN: %12d\n", CHAR_MIN);
    printf("  CHAR_MAX: %12d\n", CHAR_MAX);
    printf(" UCHAR_MAX: %12d (%d bits)\n", UCHAR_MAX, (int) round(log2(UCHAR_MAX)));

    exit(EXIT_SUCCESS);
}
