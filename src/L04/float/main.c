#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    printf("The maximum value of      float  = %.10e\n", FLT_MAX);
    printf("The minimum value of      float  = %.10e\n\n", FLT_MIN);
    printf("The maximum value of      double = %.10e\n", DBL_MAX);
    printf("The minimum value of      double = %.10e\n\n", DBL_MIN);
    printf("The maximum value of long double = %.10Le\n", LDBL_MAX);
    printf("The minimum value of long double = %.10Le\n", LDBL_MIN);

    exit(EXIT_SUCCESS);
}
