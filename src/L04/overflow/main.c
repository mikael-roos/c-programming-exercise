#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    signed short int a = SHRT_MAX;

    printf("     a = %6d (sizeof(a) = %lu)\n", a, sizeof(a));
    printf("   a++ = %6d (sizeof(a) = %lu)\n", ++a, sizeof(a));

    exit(EXIT_SUCCESS);
}
