#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

int main (int argc, char *argv[])
{
    unsigned char a;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <value>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    a = strtol(argv[1], NULL, 10);

    printf("|----------------------------|\n");
    printf("| int | hex | oct |   bits   |\n");
    printf("|-----|-----|-----|----------|\n");
    printf("| %3d | %3x | %3o | "BYTE_TO_BINARY_PATTERN" |\n",
        a,
        a,
        a,
        BYTE_TO_BINARY(a)
    );
    printf("|----------------------------|\n");

    exit(EXIT_SUCCESS);
}
