#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

int main (int argc, char *argv[])
{
    unsigned char a = 127;

    a = 0177;
    printf("%3d = "BYTE_TO_BINARY_PATTERN"\n", a, BYTE_TO_BINARY(a));

    a = 01;
    printf("%3d = "BYTE_TO_BINARY_PATTERN"\n", a, BYTE_TO_BINARY(a));

    a = 07;
    printf("%3d = "BYTE_TO_BINARY_PATTERN"\n", a, BYTE_TO_BINARY(a));

    a = 077;
    printf("%3d = "BYTE_TO_BINARY_PATTERN"\n", a, BYTE_TO_BINARY(a));

    a = 0377;
    printf("%3d = "BYTE_TO_BINARY_PATTERN"\n", a, BYTE_TO_BINARY(a));

    exit(EXIT_SUCCESS);
}
