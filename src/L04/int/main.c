#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    printf("  SHRT_MIN: %24d\n", SHRT_MIN);
    printf("  SHRT_MAX: %24d\n", SHRT_MAX);
    printf(" USHRT_MAX: %24d (%d bits)\n", USHRT_MAX, (int) round(log2(USHRT_MAX)));

    printf("   INT_MIN: %24d\n", INT_MIN);
    printf("   INT_MAX: %24d\n", INT_MAX);
    printf("  UINT_MAX: %24u (%d bits)\n", UINT_MAX, (int) round(log2(UINT_MAX)));

    printf("  LONG_MIN: %24ld\n", LONG_MIN);
    printf("  LONG_MAX: %24ld\n", LONG_MAX);
    printf(" ULONG_MAX: %24lu (%d bits)\n", ULONG_MAX, (int) round(log2(ULONG_MAX)));

    printf(" LLONG_MIN: %24lld\n", LLONG_MIN);
    printf(" LLONG_MAX: %24lld\n", LLONG_MAX);
    printf("ULLONG_MAX: %24llu (%d bits)\n", ULLONG_MAX, (int) round(log2(ULLONG_MAX)));

    exit(EXIT_SUCCESS);
}
