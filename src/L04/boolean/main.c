#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main (int argc, char *argv[])
{
    bool a = true;
    bool b = false;

    printf("a = %d (size = %lu)\n", a, sizeof(a));
    printf("b = %d (size = %lu)\n", b, sizeof(b));

    exit(EXIT_SUCCESS);
}
