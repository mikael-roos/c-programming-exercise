#include <stdio.h>
#include <stdlib.h>

#define MAX_SOMETHING 1337

const int a = 42;
const char b = 'X';

enum vote { NO, YES };
enum month { JAN=1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC };

int main (int argc, char *argv[])
{
    enum vote c = YES;
    enum month d = JUL;

    printf(
        "#define MAX_SOMETHING = %d (size = %lu)\n",
        MAX_SOMETHING, sizeof(MAX_SOMETHING)
    );
    printf("const int  a = %d (size = %lu)\n", a, sizeof(a));
    printf("const char b = %c  (size = %lu)\n", b, sizeof(b));
    printf(" enum vote c = %d  (size = %lu)\n", c, sizeof(c));
    printf("enum month d = %d  (size = %lu)\n", d, sizeof(d));

    exit(EXIT_SUCCESS);
}
