Homework assignment
=======================

This is the voluntary homework assignment that will let you practice small exercises and learn C programming from examples.

[[_TOC_]]



Try out and inspect the example programs
-----------------------

Try out the example programs in this folder. Compile them, exeute them.

Inspect their source code, can you understand all parts of the code and can you understand what the program is doing?

Feel free to copy them and try to make your own updates to them.



Read and guess a value
-----------------------

Make a program "guess" where you enter a min value and a max value. The program will then randomize a number between those numbers.

Then the program asks you to "guess what number I got". You can then enter one guess and the program will directly show its random value.



Minimum and maxmimum value
-----------------------

Create a program "minmax" that prints the MIN and MAX value for the types char, short, int, long, long long, float, double and long double.

These values you can find as constants in the headerfile `limits.h` and `float.h`.

Do also write out the number of bytes each type can hold.

Make the output in a good looking table format.



Bit calculation
-----------------------

Write a program taking 2 unsigned chars as input and then prints them out showing their representations in bits.

Then do bit calculation using "and", "or" and "xor" between the two values and print out the result in integer, oct, hex and bit representation.



Bit representation
-----------------------

Create a program "bits" that reads a integer between 0 and 65,536 from the user and then prints the value in bits, hex and oct format. It will then be a integer to bit/hex/oct converter program you can use to show the bits of a particular integer value value.
