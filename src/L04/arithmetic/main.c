#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    short year = 1968;

    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        printf("The year %d is a leap year.\n", year);
    } else {
        printf("The year %d is not a leap year.\n", year);
    }

    exit(EXIT_SUCCESS);
}
