#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    char val1;
    int val2;
    float val3;
    char val4[6];

    printf("Enter a character:\n");
    scanf("%c", &val1);
    printf("You entered a value = %c\n", val1);

    printf("Enter a integer value:\n");
    scanf("%d", &val2);
    printf("You entered a value = %d\n", val2);

    printf("Enter a float value:\n");
    scanf("%f", &val3);
    printf("You entered a value = %f\n", val3);

    printf("Enter a string, max 5 characters long:\n");
    scanf("%s", val4);
    printf("You entered a value = %s\n", val4);

    exit(EXIT_SUCCESS);
}
