#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

typedef struct {
    unsigned int BRAKE_PAD_CONDITION : 1;
    unsigned int BRAKE_FLUID_LEVEL   : 1;
    unsigned int ENGINE_TEMP         : 1;
    unsigned int ENGINE_OIL_LEVEL    : 1;
    unsigned int FUEL_LEVEL          : 1;
} Car;

void printIt(u_int flags);

int main (int argc, char *argv[])
{
    Car car;

    car.BRAKE_PAD_CONDITION = 1;
    car.BRAKE_FLUID_LEVEL = car.ENGINE_TEMP = 1;

    car.FUEL_LEVEL = car.ENGINE_OIL_LEVEL = 0;

    if (car.BRAKE_PAD_CONDITION == 1
        && car.BRAKE_FLUID_LEVEL == 1
        && car.ENGINE_TEMP == 1) {
        printf("Warning. Bad conditions.");
    }

    exit(EXIT_SUCCESS);
}
