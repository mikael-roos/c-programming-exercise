#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct point {
    char value;
    int posX;
    int posY;
};

const int gameRows = 5;
const int gameCols = 5;
const int gameSize = gameRows * gameCols;

void printGame(struct point *game[]);

int main (int argc, char *argv[])
{
    struct point *game[gameSize];
    int numberTaken = 0;

    memset(game, 0, sizeof(game));

    while (numberTaken < gameSize) {
        struct point *placeIt;
        char value;
        int posX, posY;
        int res;

        printf("Place your move (X/O posX posY)\n");
        scanf("%c %d %d", &value, &posX, &posY);
        while(getc(stdin) != '\n');

        if (value == 'q') {
            break;
        } else if (value == 'p') {
            printGame(game);
            continue;
        }
        placeIt = malloc(sizeof(struct point));
        placeIt->value = value;
        placeIt->posX = posX - 1;
        placeIt->posY = posY - 1;
        game[numberTaken++] = placeIt;

        printGame(game);
    }

    exit(EXIT_SUCCESS);
}

void printGame(struct point *game[])
{
    char matrix[gameRows][gameCols];

    memset(matrix, 0, sizeof(matrix));

    // Translate struct point game[] to two dimensional array,
    // just to make it easier to print.
    for (int i = 0; i < gameSize; i++) {
        if (game[i] == 0 || game[i]->value == 0) {
            break;
        }
        matrix[game[i]->posX][game[i]->posY] = game[i]->value;
    }

    // Print the game plan using the matrix
    printf(" | 1 | 2 | 3 | 4 | 5 |\n");
    printf("-|---|---|---|---|---|\n");

    for (int row = 0; row < gameRows; row++) {
        printf("%d|", row + 1);
        for (int col = 0; col < gameCols; col++) {
            printf(" %c |", matrix[row][col] ? matrix[row][col] : '-');
        }
        printf("\n");
    }
}
