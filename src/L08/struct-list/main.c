#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct point {
    char value;
    int posX;
    int posY;
};

struct element {
    struct point *data;
    struct element *next;
};

const int gameRows = 5;
const int gameCols = 5;

void printGame(struct element*);

int main (int argc, char *argv[])
{
    struct element *linkedList = 0;
    // int numberTaken = 0;
    //
    // memset(game, 0, sizeof(game));

    // while (numberTaken < gameSize) {
    while (1) {
        struct element *newElement;
        char value;
        int posX, posY;

        // Get a new move
        printf("Place your move (X/O posX posY)\n");
        scanf("%c %d %d", &value, &posX, &posY);
        while(getc(stdin) != '\n');

        if (value == 'q') {
            break;
        } else if (value == 'p') {
            printGame(linkedList);
            continue;
        }

        // Create a new element
        newElement = malloc(sizeof(struct element));
        newElement->next = 0;
        newElement->data =  malloc(sizeof(struct point));
        newElement->data->value = value;
        newElement->data->posX = posX - 1;
        newElement->data->posY = posY - 1;

        // Add the new element to the list
        if (linkedList != 0) {
            newElement->next = linkedList;
        }
        linkedList = newElement;

        printGame(linkedList);
    }

    exit(EXIT_SUCCESS);
}

void printGame(struct element *game)
{
    char matrix[gameRows][gameCols];

    memset(matrix, 0, sizeof(matrix));

    // Translate the game structure to a two dimensional array,
    // just to make it easier to print.
    while (game) {
        matrix[game->data->posX][game->data->posY] = game->data->value;
        game = game->next;
    }

    // Print the game plan using the matrix
    printf(" | 1 | 2 | 3 | 4 | 5 |\n");
    printf("-|---|---|---|---|---|\n");

    for (int row = 0; row < gameRows; row++) {
        printf("%d|", row + 1);
        for (int col = 0; col < gameCols; col++) {
            printf(" %c |", matrix[row][col] ? matrix[row][col] : '-');
        }
        printf("\n");
    }
}
