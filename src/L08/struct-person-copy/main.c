#include <stdio.h>
#include <stdlib.h>

struct person {
    char* surName;
    char* lastName;
    int age;
};

void printPerson(struct person aPerson);

int main (int argc, char *argv[])
{
    struct person mos = {"Mikael", "Roos", 42};
    struct person cinna = mos;

    cinna.surName = "Cinna";
    cinna.age -= 3;

    printPerson(mos);
    printPerson(cinna);

    exit(EXIT_SUCCESS);
}

void printPerson(struct person aPerson)
{
    printf("Details on person:\n Name: %s %s (%d)\n",
        aPerson.surName, aPerson.lastName, aPerson.age
    );
}
