#include <stdio.h>
#include <stdlib.h>
#include <string.h>

union Data {
    char aChar;
    int aInt;
    float aFloat;
};

int main (int argc, char *argv[])
{
    union Data data;

    data.aChar = 'X';
    printf("data -> %c\n", data.aChar);

    data.aInt = 42;
    printf("data -> %d\n", data.aInt);

    data.aFloat = 13.37;
    printf("data -> %f\n", data.aFloat);

    exit(EXIT_SUCCESS);
}
