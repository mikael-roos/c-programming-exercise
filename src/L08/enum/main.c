#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

enum {
    BRAKE_PAD_CONDITION = 1 << 0,
    BRAKE_FLUID_LEVEL   = 1 << 1,
    ENGINE_TEMP         = 1 << 2,
    ENGINE_OIL_LEVEL    = 1 << 3,
    FUEL_LEVEL          = 1 << 4,
};

void printIt(u_int flags);

int main (int argc, char *argv[])
{
    unsigned int car = 0;

    car |= FUEL_LEVEL;
    printIt(car);

    // car = 0;
    // printIt(car);

    car |= ENGINE_OIL_LEVEL;
    printIt(car);

    car |= BRAKE_PAD_CONDITION | BRAKE_FLUID_LEVEL;
    printIt(car);

    // car |= BRAKE_FLUID_LEVEL;
    // printIt(car);

    car |= ENGINE_TEMP;
    printIt(car);

    // car |= ENGINE_TEMP | BRAKE_PAD_CONDITION;
    // printIt(car);
    //
    // car |= BRAKE_FLUID_LEVEL;
    // printIt(car);

    car &= ~ENGINE_OIL_LEVEL;
    printIt(car);

    car &= ~ENGINE_TEMP;
    printIt(car);

    car &= ~(BRAKE_FLUID_LEVEL | BRAKE_PAD_CONDITION);
    printIt(car);

    // car &= ~BRAKE_PAD_CONDITION;
    // printIt(car);

    exit(EXIT_SUCCESS);
}

void printIt(u_int flags)
{
    printf("| %3d | %3x | %3o | "BYTE_TO_BINARY_PATTERN" |\n",
        flags,
        flags,
        flags,
        BYTE_TO_BINARY(flags)
    );
}
