#include <stdio.h>
#include <stdlib.h>

struct person {
    char* surName;
    char* lastName;
    int age;
};

void printPerson(struct person aPerson);

int main (int argc, char *argv[])
{
    struct person mos = {"Mikael", "Roos", 42};
    struct person doe;

    doe.surName = "Johan/Jane";
    doe.lastName = "Doe";
    doe.age = 1337;

    printPerson(mos);
    printPerson(doe);

    exit(EXIT_SUCCESS);
}

void printPerson(struct person aPerson)
{
    printf("Details on person:\n Name: %s %s (%d)\n",
        aPerson.surName, aPerson.lastName, aPerson.age
    );
}
