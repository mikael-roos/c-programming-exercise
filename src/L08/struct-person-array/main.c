#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person {
    char surName[100];
    // char *surName;
    char lastName[20];
    int age;
};

void printPerson(struct person aPerson);

int main (int argc, char *argv[])
{
    struct person mos = {"Mikael", "Roos", 42};
    struct person doe;
    char *name1 = "John/Jane";

    // doe.surName = name1;
    strcpy(name1, doe.surName);
    doe.lastName = "Doe";
    doe.age = 1337;

    printPerson(mos);
    printPerson(doe);

    exit(EXIT_SUCCESS);
}

void printPerson(struct person aPerson)
{
    printf("Details on person:\n Name: %s %s (%d)\n",
        aPerson.surName, aPerson.lastName, aPerson.age
    );
}
