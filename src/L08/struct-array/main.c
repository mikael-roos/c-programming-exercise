#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct point {
    char value;
    int posX;
    int posY;
};

const int gameRows = 5;
const int gameCols = 5;
const int gameSize = gameRows * gameCols;

void printGame(struct point game[]);

int main (int argc, char *argv[])
{
    struct point game[gameSize];

    game[0] = (struct point) {'X', 1, 1};
    game[1] = (struct point) {'O', 0, 4};
    game[2] = (struct point) {'X', 1, 2};
    game[3] = (struct point) {'O', 1, 4};
    game[4] = (struct point) {'X', 2, 2};
    game[5] = (struct point) {'O', 4, 4};

    printGame(game);

    exit(EXIT_SUCCESS);
}

void printGame(struct point game[])
{
    char matrix[gameRows][gameCols];

    memset(matrix, 0, sizeof(matrix));

    // Translate struct point game[] to two dimensional array,
    // just to make it easier to print.
    for (int i = 0; i < gameSize; i++) {
        if (game[i].value == 0) {
            break;
        }
        matrix[game[i].posX][game[i].posY] = game[i].value;
    }

    // Print the game plan using the matrix
    printf(" | 1 | 2 | 3 | 4 | 5 |\n");
    printf("-|---|---|---|---|---|\n");

    for (int row = 0; row < gameRows; row++) {
        printf("%d|", row + 1);
        for (int col = 0; col < gameCols; col++) {
            printf(" %c |", matrix[row][col] ? matrix[row][col] : '-');
        }
        printf("\n");
    }
}
