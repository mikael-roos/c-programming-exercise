#include <stdio.h>
#include <stdlib.h>

struct person {
    char* surName;
    char* lastName;
    int age;
};

void printPerson(struct person aPerson);

void updatePersonValue(struct person aPerson);

struct person updatePersonReturn(struct person aPerson);

void updatePersonPointer(struct person *aPerson);

int main (int argc, char *argv[])
{
    struct person mos = {"Mikael", "Roos", 42};

    printPerson(mos);
    //updatePersonvalue(mos);
    //mos = updatePersonReturn(mos);
    updatePersonPointer(&mos);
    printPerson(mos);

    exit(EXIT_SUCCESS);
}

void updatePersonValue(struct person aPerson)
{
    aPerson.surName = "Mega Mic";
    aPerson.age = 1337;
}

struct person updatePersonReturn(struct person aPerson)
{
    aPerson.surName = "Mega Mic";
    aPerson.age = 1337;

    return aPerson;
}

void updatePersonPointer(struct person *aPerson)
{
    // aPerson->surName = "Mega Mic";
    // aPerson->age = 1337;
    (*aPerson).surName = "Mega Mic";
    (*aPerson).age = 1337;
}

void printPerson(struct person aPerson)
{
    printf("Details on person:\n Name: %s %s (%d)\n",
        aPerson.surName, aPerson.lastName, aPerson.age
    );
}
