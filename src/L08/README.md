Homework assignment
=======================

This is the voluntary homework assignment that will let you practice small exercises and learn C programming from examples.

[[_TOC_]]



Try out and inspect the example programs
-----------------------

Try out the example programs in this folder. Compile them, exeute them.

Inspect their source code, can you understand all parts of the code and can you understand what the program is doing?

Feel free to copy them and try to make your own updates to them.



Add features to your bot
-----------------------

Improve your program by adding more features to your bot program. Try to add the following features.

```
$ ./a.out
This is your personal bot "Marvin" that can anwer anything.
     (____)
     (_oo_)
       (O)
     __||__     )
  []/_______[] /
  / ________/ /
 /    /___/
(    /___/

Menu
-----------------------------------------

16. Show the content of your backpack (backpack)
17. Add a item to your backpack (backpack-add)
18. Remove a item from your backpack (backpack-remove)
19. Remove all items from the backpack (backpack-empty)

-----------------------------------------

Enter your choice (or 'q' to quit):
```

Store all these functions in their own module using `.h` and `.c` files.



### 16. Show the content of your backpack

Create a linked list that becomes your "backpack" where you can store items.

You can freely define the structure of the elements to be stored in your backpack but you must allocate the memory dynamically.

Print out the content of the backpack.



### 17. Add a item to your backpack

Ask the user for a details about the item you want to add to your backpack and add it.



### 18. Remove a item from your backpack

Remove an item from the backpack. Perhaps you need to implement a key for each item so the user can enter the specific key to remove a specific item from the list.

Do not forget to free the memory allocated for the item.



### 19. Remove all items from the backpack

Remove all item from the backpack to make it empty. Do not forget to free the memory allocated for the items.
