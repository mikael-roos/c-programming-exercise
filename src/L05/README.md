Homework assignment
=======================

This is the voluntary homework assignment that will let you practice small exercises and learn C programming from examples.

[[_TOC_]]



Try out and inspect the example programs
-----------------------

Try out the example programs in this folder. Compile them, exeute them.

Inspect their source code, can you understand all parts of the code and can you understand what the program is doing?

Feel free to copy them and try to make your own updates to them.



If, else if, else
-----------------------

Write a program "if" that reads a value from the user.

If the value is divisable by three then the program should output "Fizz".

If the value is divisable by five it should output "Buzz".

If the value is divisable by fifteen it should output "Fizz Buzz".



Check if valid month
-----------------------

Take the example program "switch" and make it as an eternal loop, reading from the terminal a value.

If the value matches a month, then print the months name.

If the value matches 0, then exit the program.

If the value matches 42 then say "You found the answer!".



Loop it
-----------------------

Write a program "loop" where you ask the user for a "start" and an "end" value.

Then create three loops in your program. One for, one while and one do-while loop.

Each of the loops should run from "start" to "end" and printing out the value for each loop round.



Histogram
-----------------------

Write a program "histogram" that rolls one dice and shows it results. It then asks the users wether to roll again or not. The user enters a 'q' to quit.

After each roll a histogram is printed showing the how many 1, 2, 3, 4, 5and 6 has been throwh during the game.

You should also calculate and show the total sum f the dices and the average of all dices.

A histogram of a dice serie 1, 2, 2, 3, 3, 3, 4, 4, 5, 6, 6, 6 can be represented by a histogram likes this.

1. *
2. **
3. ***
4. **
5. *
6. ***



Ascii art random
-----------------------

Create a program "asciirandom" where you can enter a character and a number for the columns and a number for the rows. The program will then print out that character for the number of columns wide and the number of rows long.

If will furthermore use some randomizing so that every second or third character is not printed out. So you never know what kind of output you will get.

This is an example for char a, col 3 and row 3.

```
aa
a a
 aa
```

Then you enhance the program in randomizing the character being printed so occasionally another character is being printed instead of the choosen one.



Fizz Buzz game
-----------------------

Teach the computer to play a game of [Fizz Buzz](https://en.wikipedia.org/wiki/Fizz_buzz). Write a program ""fizzbuzz" where you play against the computer.

Since the computer will calculate the accurate response each time you will need to use some randomizing so that the computer as a probability of, say 80%, to provide a wrong guess each time.

Play against the computer so it starts, then you answer the next by writing the response in the terminal, the computer checks if it is correct, then the computer answerson the next value itself, and checks if it is correct and then it continues until you or the computer fails to answer correctly.
