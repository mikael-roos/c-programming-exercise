#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    int a;
    char *str;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <value>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    a = strtol(argv[1], NULL, 10);

    switch (a) {
        case 1: str = "January"; break;
        case 2: str = "February"; break;
        case 3: str = "March"; break;
        default: str = "No such month";
    }

    printf("The month number %d is %s.\n", a, str);

    exit(EXIT_SUCCESS);
}
