#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    FILE *fp;
    char *filename = "file.txt";
    char ch;

    // // Open the file with error handling
    // fp = fopen("nosuchfile.txt", "r");
    // // if(fp == NULL) {
    // //     perror("Error opening file");
    // //     return(-1);
    // // }
    // fclose(fp);

    // Create and make the file empty
    fp = fopen(filename, "w");
    fclose(fp);

    // Open the file for writing
    fp = fopen(filename, "w");
    fputs("This is C-programming with files.\n", fp);
    fclose(fp);

    // Open the file for reading
    fp = fopen(filename, "r");
    while ((ch = getc(fp)) != EOF ) {
        putchar(ch);
    }
    fclose(fp);

    printf("---\n");

    // Open the file for append
    fp = fopen(filename, "a");
    fputs("Thisasasas is C-programming with files.\n", fp);
    fclose(fp);

    // Open the file for reading
    fp = fopen(filename, "r");
    while ((ch = getc(fp)) != EOF ) {
        putchar(ch);
    }
    fclose(fp);

    exit(EXIT_SUCCESS);
}
