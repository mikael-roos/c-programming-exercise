#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    int *ch;

    while ((ch = getchar()) != EOF ) {
        putchar(tolower(ch));
    }

    exit(EXIT_SUCCESS);
}
