Homework assignment
=======================

This is the voluntary homework assignment that will let you practice small exercises and learn C programming from examples.

[[_TOC_]]



Try out and inspect the example programs
-----------------------

Try out the example programs in this folder. Compile them, exeute them.

Inspect their source code, can you understand all parts of the code and can you understand what the program is doing?

Feel free to copy them and try to make your own updates to them.



Add features to your bot
-----------------------

Improve your program by adding more features to your bot program. Try to add the following features.

```
$ ./a.out
This is your personal bot "Marvin" that can anwer anything.
     (____)
     (_oo_)
       (O)
     __||__     )
  []/_______[] /
  / ________/ /
 /    /___/
(    /___/

Menu
-----------------------------------------

20. Save backpack to file (backpack-save)
21. Load the backpack from a file (backpack-load)
22. Sort the items in the backpack (backpack-sort)

-----------------------------------------

Enter your choice (or 'q' to quit):
```

Store all these functions in their own module using `.h` and `.c` files.



### 20. Save backpack to file

Take the backpack as it is and save it, all its elements, to a file. The user can name the file and there should be one standard filename that is preselected.



### 21. Load the backpack from a file

Load the backpack from a fileThe user can enter the filename to load and there is a default filename preselected.



### 22. Sort the items in the backpack

Make it possible so all items in the backpack can be sorted. There must be some key to sort by, but you can choose that on your own. Either select a key and alwasy sort by that, or let the user select what key to sort by. This partially depends on the structure you have for the elements in the backpack.
