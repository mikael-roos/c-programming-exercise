#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    FILE *fp;
    char *filename = "file.txt";
    char c;

    fp = fopen(filename, "w");

    c = fgetc(fp);
    if(ferror(fp)) {
       fprintf(stderr, "Error in reading from file: '%s'\n", filename);
    }
    clearerr(fp);

    if(ferror(fp)) {
        fprintf(stderr, "Error (still) in reading from file: '%s'\n", filename);
    }
    fclose(fp);

    exit(EXIT_SUCCESS);
}
