#include <stdio.h>
#include <stdlib.h>

typedef struct person {
    char surName[20];
    char lastName[20];
    int age;
} Person;

int main (int argc, char *argv[])
{
    FILE *fp;
    char *filename = "file.txt";
    Person mos = {"Mikael", "Roos", 42};
    Person doe = {"John/Jane", "Doe", 1337};
    size_t bytes;

    fp = fopen(filename, "w");
    fwrite(&mos, sizeof(Person), 1, fp);
    fwrite(&doe, sizeof(Person), 1, fp);
    fclose(fp);

    //system("cat file.txt");

    exit(EXIT_SUCCESS);
}
