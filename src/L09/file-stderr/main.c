#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    FILE *fp;
    char *filename;

    // Open the file with error handling
    filename = "nosuchfile.txt";
    fp = fopen(filename, "r");
    if(fp == NULL) {
        fprintf(stderr, "Error opening file: '%s'\n", filename);
        exit(EXIT_FAILURE);
    }
    fclose(fp);

    exit(EXIT_SUCCESS);
}
