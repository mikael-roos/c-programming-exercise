Homework assignment
=======================

This is the voluntary homework assignment that will let you practice small exercises and learn C programming from examples.

[[_TOC_]]



Try out and inspect the example programs
-----------------------

Try out the example programs in this folder. Compile them, exeute them.

Inspect their source code, can you understand all parts of the code and can you understand what the program is doing?

Feel free to copy them and try to make your own updates to them.



Create a bot that can answer questions
-----------------------

Make a program that looks like this when you run it.

```
$ ./a.out
This is your personal bot "Marvin" that can anwer anything.
     (____)
     (_oo_)
       (O)
     __||__     )
  []/_______[] /
  / ________/ /
 /    /___/
(    /___/

Menu
-----------------------------------------

1. Calculate Celcius from Farenheit
2. Calculate Farenheit from Celcius
3. Calculate leap year
4. Print bit representation from a value

-----------------------------------------

Enter your choice (or 'q' to quit):
```

The main function should be small and clean. he file `main.c` should only contain the main function.

All the features behind each menu choice shall be in its own function. These functions should be stored in "modules" with a corresponding `.h` and .`c` file. You may create one or several modules.

Each menu choice should ask the user for input and then do the proper calculation and then return back to the main loop. The loop should be in its own function and not in the main function.



What number am I thinking of?
-----------------------

Add another feature to you personal bot. This feature should be added in its own module with itw own `.h` and `.c` file.

```
5. Guess my number
```

This is a game where the computer is guessing on a number between 1 and 100. You can guess the number and the computer answers with "Correct!", "Too low" or "To high". You can continue to guess until you guessed right or until you run out of guesses. Max guesses could be 5, or 7 for a bit easier game to play.
