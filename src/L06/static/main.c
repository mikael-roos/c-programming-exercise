#include <stdio.h>

void memory () {
    static int inc = 0;

    printf("%d\n", inc += 10);
}

int main (int argc, char *argv[])
{
    for (int i = 0; i < 7; i++) {
        memory();
    }
}
