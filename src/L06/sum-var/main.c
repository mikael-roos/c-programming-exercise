#include <stdarg.h>
#include <stdio.h>

int sum (int argc, ...) {
    int aSum = 0;
    va_list ap;

    va_start(ap, argc);
    for(int i=0; i< argc; i++) {
        aSum += va_arg(ap, int);
    }
    va_end(ap);

    return aSum;
}

int main ()
{
    int k = 1;
    int l = 2;
    int m = 3;

    printf("Sum is %d\n", sum(5, k, l, m, 6, 12));
}
