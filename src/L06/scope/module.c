#include <stdio.h>

#include "module.h"

const char* str_MODULE_H = "module.h";
const char* str_MODULE_C = "module.c";

extern char* str_MAIN;

void someFunc () {
    printf("%s\n", str_MAIN);
    printf("%s\n", str_MODULE_H);
    printf("%s\n", str_MODULE_C);
}
