#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    printf("Have a nice weekend!\n");

    return(42);
}

// Smallest function, return void/nothing
void dummy () {}

// May return value
int dummy2 () {
    return 42;
}

// Type whats returned
float dummy3 () {
    return 3.141592654;
}

char dummy4 () {
    return 'Z';
}
