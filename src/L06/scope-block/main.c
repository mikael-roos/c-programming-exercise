#include <stdio.h>

int main ()
{
    int i = 42;

    printf("Outside %d\n", i);

    for (int i = 0; i <= 2; i++) {
        printf("Inside %d\n", i);
    }

    printf("Outside %d\n", i);
}
