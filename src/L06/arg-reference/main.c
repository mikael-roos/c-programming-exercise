#include <stdio.h>

void update (int* a, int* b) {
    (*a)++;
    (*b)--;
}

int main ()
{
    int k = 2;
    int l = 4;

    update(&k, &l);
    update(&k, &l);

    printf("values are %d %d\n", k, l);
}
